﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        //private static IEnumerable<HtmlNode> GetDivElementsWithClasses(HtmlDocument doc, IEnumerable<String> classNames)
        //{

        //    String selector = "div." + String.Join(".", classNames);

        //    return doc.QuerySelectorAll(selector);
        //}
        static void Main(string[] args)
        {
            //Console.ReadLine();


            void _main(string year)
            {
                var url = "http://vstup.info";
                var web = new HtmlWeb();
                var webC = new HtmlDocument();
                var globaliterator = 0;
                //var doc = cachedView(url);
                var doc = web.Load(url);
                Console.WriteLine(year);
                Dictionary<string, string> cities = new Dictionary<string, string>();
                doc.GetElementbyId((((year == "2018") || (year == "2017")) ? year : "2018") + "abet")
                .ChildNodes.Where(x => (x.Name == "tbody")).First()
                .ChildNodes.Where(x => x.Name == "tr").ToList()
                .ForEach(x => x.ChildNodes
                    .Where(y => y.Name == "td")
                    .ToList().ForEach(a =>
                    {
                        var att = a.ChildNodes.First().Attributes;
                        if (!att["href"].Value.Contains("out"))
                            cities.Add(a.InnerText, url + att["href"].Value.Replace("2018", year));
                    }
                    ));
                //cities.ToList().ForEach(p => Console.WriteLine(p.Key + "  ---  " + p.Value));
                Console.WriteLine(year + " cities " + cities.Count);

                //var univers = new Dictionary<Dictionary<string, string>, string>();
                var univers = new List<Dictionary<string, string>>();

                cities.ToList().ForEach(cit =>
                {

                    //var curcity = cities.First().Value;
                    var curcity = cit.Value;
                    var city = cachedView(curcity, year + "\\" + cit.Key + "\\");
                    void doUniv(string nm)
                    {
                        if (city.Text.Contains(nm))
                            city.GetElementbyId(nm).ChildNodes[1].ChildNodes.Where(x => x.Name == "tr").ToList().ForEach(x =>
                            {
                                var y = x.ChildNodes[0].ChildNodes[0].Attributes;
                                var paramss = new Dictionary<string, string>();
                                paramss.Add("title", y["title"].Value);
                                paramss.Add("city", cit.Key);
                                paramss.Add("url", url + y["href"].Value.Replace("./", "/" + year + "/"));
                                univers.Add(paramss);
                            });
                    }
                    doUniv("vnzt0");
                    doUniv("vnzt1");
                    doUniv("vnzt2");
                    doUniv("vnzt3");
                    doUniv("vnzt4");
                    doUniv("vnzt5");
                });

                //univers.ToList().ForEach(p => Console.WriteLine(p.Key + "  ---  " + p.Value));
                Console.WriteLine(year + " univers " + univers.Count);

                // List<string> specs = new List<string>();
                //var specs = new Dictionary<Dictionary<string, string>, string>();
                var specs = new List<Dictionary<string, string>>();


                univers.ToList().ForEach(mm =>
                {

                    //var curuniv = univers.First().Value;
                    var curuniv = mm["url"];
                    var city = mm["city"];
                    //pr_paramss[""];
                    var univ = cachedView(curuniv, year + "\\" + city + "\\");
                    void doSpec(string denzao)
                    {
                        if (univ.Text.Contains(denzao))
                        {
                            var tmpr = univ.GetElementbyId(denzao);
                            //univ.GetElementbyId(denzao).ChildNodes[5].ChildNodes.Where(x => (x.Name == "tr") && (x.ChildNodes[0].InnerText.Contains("ПЗСО"))).ToList().ForEach(x =>
                            univ.GetElementbyId(denzao).ChildNodes[5].ChildNodes.Where(x => (x.Name == "tr")).ToList().ForEach(x =>
                           {
                               var paramss = new Dictionary<string, string>();
                               string y = "";
                               bool dothis = false;
                               if (year == "2018")
                               {
                                   y = x.ChildNodes[1].ChildNodes[1].Attributes["href"].Value;
                                   dothis = true;
                               }
                               else
                               {
                                   var nn = x.ChildNodes[1].ChildNodes.Where(xx => xx.Attributes.Contains("href"));
                                   var nnn = nn.ToList();
                                   if (nnn.Count > 0)
                                   {
                                       y = nn.First().Attributes["href"].Value;
                                       dothis = true;
                                   }
                               }
                               if (dothis)
                               {
                                   paramss.Add("url", url + y.Replace("./", "/" + year + "/"));
                                   paramss.Add("city", city);

                                   specs.Add(paramss);
                               }
                           });
                        }
                    }

                    doSpec("denna1");
                    doSpec("denna2");
                    doSpec("denna3");
                    doSpec("denna4");
                    doSpec("denna5");
                    doSpec("zaoch1");
                    doSpec("zaoch2");
                    doSpec("zaoch3");
                    doSpec("zaoch4");
                    doSpec("zaoch5");
                    //doSpec("zaoch5");
                    //doSpec("zaoch5");
                    //doSpec("zaoch5");
                    //doSpec("zaoch5");
                    //doSpec("zaoch5");

                });

                //specs.ForEach(p => Console.WriteLine(p));
                Console.WriteLine(year + " specs " + specs.Count);

                //specs.ForEach(p =>
                Parallel.ForEach(specs, new ParallelOptions { MaxDegreeOfParallelism = 1 }, p =>
                {
                    //var name = p.Replace(":", "").Replace("/", "\\").Replace("?", "");
                    //if (!File.Exists(name))
                    //{
                    //    var sp = web.Load(p);

                    //    writedoc(p, sp.Text);
                    //}
                    var url_ = year + "\\" + p["city"] + "\\" + p["url"].Replace(":", "").Replace("/", "").Replace("?", "");
                    if (!File.Exists(url_))
                        cachedView(p["url"], year + "\\" + p["city"] + "\\");
                    globaliterator++;
                    if (globaliterator.ToString().EndsWith("000"))
                        Console.WriteLine(year + " " + globaliterator + " Processed");

                });

                Console.WriteLine(year + " Done!");

                void writedoc(string p, string s)
                {
                    var t = p.Replace(":", "").Replace("/", "\\").Replace("?", "");
                    FileInfo file = new FileInfo(t);
                    if (!file.Directory.Exists) file.Directory.Create(); // If the directory already exists, this method does nothing.
                    File.WriteAllText(file.FullName, s);
                }


                HtmlDocument cachedView(string surl, string path)
                {
                    var url_ = path + surl.Replace(":", "").Replace("/", "").Replace("?", "");
                    if (!File.Exists(url_))
                    {
                        var content = web.Load(surl);
                        writedoc(url_, content.Text);
                        return content;
                    }
                    else
                    {
                        var doclocal = new HtmlDocument();
                        doclocal.LoadHtml(File.ReadAllText(url_, Encoding.UTF8));
                        return doclocal;
                    };
                }
            }

            void _Oldmain(string year)
            {
                var url = "http://vstup.info";
                var web = new HtmlWeb();
                // web.OverrideEncoding = Encoding.GetEncoding(1251);
                var webC = new HtmlDocument();
                var globaliterator = 0;
                //var doc = cachedView(url);
                var doc = web.Load(url);
                Console.WriteLine(year);
                Dictionary<string, string> cities = new Dictionary<string, string>();
                doc.GetElementbyId((((year == "2018") || (year == "2017")) ? year : "2018") + "abet")
                .ChildNodes.Where(x => (x.Name == "tbody")).First()
                .ChildNodes.Where(x => x.Name == "tr").ToList()
                .ForEach(x => x.ChildNodes
                    .Where(y => y.Name == "td")
                    .ToList().ForEach(a =>
                    {
                        var att = a.ChildNodes.First().Attributes;
                        if (!att["href"].Value.Contains("out"))
                            cities.Add(a.InnerText, url + att["href"].Value.Replace("2018", year));
                    }
                    ));
                //cities.ToList().ForEach(p => Console.WriteLine(p.Key + "  ---  " + p.Value));
                Console.WriteLine(year + " cities " + cities.Count);

                //var univers = new Dictionary<Dictionary<string, string>, string>();
                var univers = new List<Dictionary<string, string>>();

                cities.ToList().ForEach(cit =>
                {



                    //var curcity = cities.First().Value;
                    var curcity = cit.Value;
                    var city = cachedView(curcity, year + "\\" + cit.Key + "\\");
                    var findclasses = city.DocumentNode.Descendants()
                        .Where(d => d.Attributes.Contains("id") && d.GetAttributeValue("id", "").Contains("branch2"));
                    var hrefs = findclasses.SelectMany(x => x.ChildNodes.Where(c => c.Name == "a").Where(ti => ti.GetAttributeValue("title", "") != "Опитування")
                        .SelectMany(v => v.Attributes.Where(b => b.Name == "href"))).Select(x => url + x.Value.Replace("./", "/" + year + "/"));
                    hrefs.ToList().ForEach(x =>
                    {
                        var paramss = new Dictionary<string, string>();
                        paramss.Add("city", cit.Key);
                        paramss.Add("url", x);
                        univers.Add(paramss);

                    });
                });
                //univers.ToList().ForEach(p => Console.WriteLine(p.Key + "  ---  " + p.Value));
                Console.WriteLine(year + " univers " + univers.Count);

                // List<string> specs = new List<string>();
                //var specs = new Dictionary<Dictionary<string, string>, string>();
                var specsUrls = new List<Dictionary<string, string>>();
                var specs = new List<Dictionary<string, string>>();



                if (year!="2009")
                univers.ToList().ForEach(mm =>
                {

                    //var curuniv = univers.First().Value;
                    var curuniv = mm["url"];
                    var city = mm["city"];
                    //pr_paramss[""];
                    var aaaaa = 1;
                    var univ = cachedView(curuniv, year + "\\" + city + "\\");
                    if (univ.Text.Contains("row"))
                    {

                        var findclasses = univ.DocumentNode.Descendants()
                            .Where(d => d.Attributes.Contains("class") && d.GetAttributeValue("class", "").Contains("row") && (true)).Where(x => !x.InnerText.Contains("Опитування")).ToList().FirstOrDefault();
                        if (findclasses != null)
                        {
                            var findclasses1 = findclasses.Descendants().Where(d => d.Attributes.Contains("class") && d.GetAttributeValue("class", "").Contains("i3")).ToList();
                            if (findclasses1.Count > 0)
                            {
                                var hrefs = findclasses1.SelectMany(x => x.ChildNodes.Where(y => y.Name == "a")).Where(x => (x.InnerText != "Статистика") && (x.InnerText != "Статистика")).SelectMany(x => x.Attributes)
                                .Where(x => x.Name == "href").Select(x => url + x.Value.Replace("./", "/" + year + "/"));
                                if (hrefs.ToList().Count > 0)
                                    hrefs.ToList().ForEach(x =>
                                    {
                                        var paramss = new Dictionary<string, string>();
                                        paramss.Add("city", city);
                                        paramss.Add("url", x);
                                        specsUrls.Add(paramss);
                                        var aaaaaa = 1;
                                    });
                            }
                        }
                    }
                });

                if (year == "2009") specsUrls = univers;
                Console.WriteLine(year + " specURLs " + specsUrls.Count);


                specsUrls.ToList().ForEach(mm =>
                {

                    //var curuniv = univers.First().Value;
                    var curuniv = mm["url"];
                    var city = mm["city"];
                    //pr_paramss[""];
                    var univ = cachedView(curuniv, year + "\\" + city + "\\");
                    List<string> findclasses = new List<string>();
                    if (year=="2009")
                    {
                        univ.DocumentNode.Descendants()
                            .SelectMany(d => d.ChildNodes).Where(x => x.Name == "table").ToList().ForEach(table =>
                            {findclasses.AddRange(table.Descendants().SelectMany(d => d.ChildNodes).Where(x => x.Name == "a")
                                                                                       .SelectMany(x => x.Attributes).Where(x => x.Name == "href")
                                                                                       .Select(x => url + x.Value.Replace("./", "/" + year + "/")).ToList());
                            });
                    }
                    else if (year == "2010" || year == "2011")
                    {
                        var bfindclasses = univ.DocumentNode.Descendants()
                            .SelectMany(d => d.ChildNodes).Where(x => x.Name == "table").FirstOrDefault();
                        if (bfindclasses!=null)
                        findclasses=bfindclasses.Descendants().SelectMany(d => d.ChildNodes).Where(x => x.Name == "a")
                                                                                     .SelectMany(x => x.Attributes).Where(x => x.Name == "href")
                                                                                     .Select(x => url + x.Value.Replace("./", "/" + year + "/")).ToList();
                    }
                    else
                        findclasses = univ.DocumentNode.Descendants()
                            .SelectMany(d => d.ChildNodes).Where(x => x.Name == "a" && x.InnerText == "конкурс")
                            .SelectMany(x => x.Attributes).Where(x => x.Name == "href")
                            .Select(x => url + x.Value.Replace("./", "/" + year + "/")).ToList();
                    if (findclasses.Count > 0)
                        findclasses.ForEach(x =>
                        {
                            var paramss = new Dictionary<string, string>();
                            paramss.Add("city", city);
                            paramss.Add("url", x);
                            specs.Add(paramss);
                        });

                });



                //specs.ForEach(p => Console.WriteLine(p));
                Console.WriteLine(year + " specs " + specs.Count);

                specs.ForEach(p =>
                {
                    //var name = p.Replace(":", "").Replace("/", "\\").Replace("?", "");
                    //if (!File.Exists(name))
                    //{
                    //    var sp = web.Load(p);

                    //    writedoc(p, sp.Text);
                    //}

                    cachedView(p["url"], year + "\\" + p["city"] + "\\");
                    globaliterator++;
                    if (globaliterator.ToString().EndsWith("000"))
                        Console.WriteLine(year + " " + globaliterator + " Processed");

                });

                Console.WriteLine(year + " Done!");

                string Win1251ToUTF8(string source)
                {
                    Encoding utf8 = Encoding.GetEncoding("UTF-8");
                    Encoding win1251 = Encoding.GetEncoding("windows-1251");
                    byte[] utf8Bytes = win1251.GetBytes(source);
                    byte[] win1251Bytes = Encoding.Convert(win1251, utf8, utf8Bytes);
                    source = win1251.GetString(win1251Bytes);
                    return source;
                }

                void writedoc(string p, HtmlDocument s)
                {
                    var t = p.Replace(":", "").Replace("/", "\\").Replace("?", "");
                    FileInfo file = new FileInfo(t);
                    if (!file.Directory.Exists) file.Directory.Create(); // If the directory already exists, this method does nothing.
                    s.Save(p);
                    //File.WriteAllText(file.FullName,Win1251ToUTF8(s) );
                }


                HtmlDocument cachedView(string surl, string path)
                {
                    var url_ = path + surl.Replace(":", "").Replace("/", "").Replace("?", "");
                    if (!File.Exists(url_))
                    {
                        web.OverrideEncoding = Encoding.GetEncoding("windows-1251");
                        var content = web.Load(surl);
                        writedoc(url_, content);
                        return content;
                    }
                    else
                    {
                        var doclocal = new HtmlDocument();
                        doclocal.LoadHtml(File.ReadAllText(url_, Encoding.GetEncoding("windows-1251")));
                        return doclocal;
                    };
                }
            }


            var l = new List<Task>();
            l.Add(new Task(() => { _main("2018"); }));
            //l.Add(new Task(() => { _main("2017"); }));
            //l.Add(new Task(() => { _main("2016"); }));
            //l.Add(new Task(() => { _main("2015"); }));
            //l.Add(new Task(() => { _Oldmain("2014"); }));
            //l.Add(new Task(() => { _Oldmain("2013"); }));
            //l.Add(new Task(() => { _Oldmain("2012"); }));
            //l.Add(new Task(() => { _Oldmain("2011"); }));
            //l.Add(new Task(() => { _Oldmain("2010"); }));
            //l.Add(new Task(() => { _Oldmain("2009"); }));
            l.ForEach(x => x.Start());
            //_Oldmain("2014");
            //_Oldmain("2009");
            //_main("2017");
            //_main("2016");
            //_main("2015");
            l.ForEach(x => x.Wait());
            Console.WriteLine("Done");
            Console.ReadLine();

        }
    }
}
